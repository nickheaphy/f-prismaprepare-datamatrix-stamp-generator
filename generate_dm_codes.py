from pystrich_nh.datamatrix import DataMatrixEncoder
import sys
import logging
import os
import zipfile
import time
import math
from PIL import Image, ImageDraw, ImageFont
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))

# Version 2

#logging.getLogger("datamatrix").setLevel(logging.DEBUG)
#logging.getLogger("datamatrix").addHandler(logging.StreamHandler(sys.stdout))

REQUIRED_CELL_SIZE = 0.4 # mm
QUIET_ZONE = 7 # Cells
CELL_RESOLUTION = 10 # Pixels

IMAGE_PREFIX = "dm"

INCLUDE_TEXT = True

def main():
    '''Main function'''

    # check the output folder exists, otherwise create
    os.makedirs("output", exist_ok=True)

    # Somewhere to store the XML
    xml_out = []

    for i in range(0,90):
        barcode_string = "{0:02d}".format(i)
        encoder = DataMatrixEncoder(barcode_string)
        # figure out the resolution to set the size correctly
        num_cells_across = len(encoder.matrix) + 2 + (2 * QUIET_ZONE)
        pix_across = num_cells_across * CELL_RESOLUTION
        calc_ppi = 25.4 / (REQUIRED_CELL_SIZE * num_cells_across) * pix_across
        encoder.save("output/%s_%s.nch.png" % (IMAGE_PREFIX, barcode_string), CELL_RESOLUTION, QUIET_ZONE, calc_ppi)
        # Are we going to add the number text?
        if INCLUDE_TEXT:
            # Reopen the png and add space and text
            add_number("output/%s_%s.nch.png" % (IMAGE_PREFIX, barcode_string), barcode_string)

        xml_out.append(generate_xml_node(barcode_string, "Portrait", "SEF"))
        xml_out.append(generate_xml_node(barcode_string, "Portrait", "LEF"))
        xml_out.append(generate_xml_node(barcode_string, "Landscape", "SEF"))
        xml_out.append(generate_xml_node(barcode_string, "Landscape", "LEF"))
    
    # output the XML
    f = open("output/TemporaryStampCatalog.xml","w+")
    f.write('<?xml version="1.0" encoding="utf-8"?>\n')
    f.write('<StampCatalog Version="1">\n')
    f.write('<Stamps>\n')
    for stamp in xml_out:
        f.write(stamp)
    f.write('</Stamps>\n')
    f.write('</StampCatalog>\n')
    f.close()

    # you are feeling very sleepy
    time.sleep(5)

    # compress all the files in the output folder
    with zipfile.ZipFile('PrismaPrepare_Stamp_Catalog.zip', 'w') as myzip:
        for (dirpath, dirnames, filenames) in os.walk("output"):
            for file in filenames:
                if file.endswith(".png") or file.endswith(".xml"):
                    # print(os.path.join(dirpath, file))
                    myzip.write(os.path.join(dirpath, file), file)
                    # myzip.write(file)


def generate_xml_node(dm_number, sheet_orientation, feed_direction):
    '''Quick and dirty XML output'''
    # Really should use a XML library...
    stampname = "%s %s %s" % (dm_number, sheet_orientation, feed_direction)
    x = "<Stamp>\n"
    # x += "<Name>%s Left (Landscape)</Name>" % dm_number
    x += "<Name>%s</Name>\n" % stampname
    x += "<IsText>False</IsText>\n"
    x += "<ImageName>%s_%s.png</ImageName>\n" % (IMAGE_PREFIX, dm_number)
    x += "<ImagePath>C:\\ProgramData\\Oce\\PRISMAprepare\\Stamps\\%s_%s.nch.png</ImagePath>\n" % (IMAGE_PREFIX, dm_number)
    x += "<ImagePathEPS />\n"
    x += "<ImageExtension>PNG</ImageExtension>\n"
    x += "<OriginalUserImagePath>C:\\temp\\%s_%s.png</OriginalUserImagePath>\n" % (IMAGE_PREFIX, dm_number)
    x += "<Scale>True</Scale>\n"
    x += "<ScaleValue>100</ScaleValue>\n"
    x += "<ScaleValueVertical>100</ScaleValueVertical>\n"
    x += "<MaintainAspectRatio>False</MaintainAspectRatio>\n"
    x += "<Location>Overlay</Location>\n"
    x += "<Opacity>100</Opacity>\n"
    x += "<Rotation>Custom</Rotation>\n"
    if sheet_orientation == "Portrait" and feed_direction == "SEF":
        x += "<RotationValue>90</RotationValue>\n"
        x += "<Position>Top</Position>\n"
        x += "<HorizontalShift>[80;1;Millimeters]</HorizontalShift>\n"
        x += "<VerticalShift>[0;1;Millimeters]</VerticalShift>\n"
    elif sheet_orientation == "Portrait" and feed_direction == "LEF":
        x += "<RotationValue>0</RotationValue>\n"
        x += "<Position>Left</Position>\n"
        x += "<HorizontalShift>[0;1;Millimeters]</HorizontalShift>\n"
        x += "<VerticalShift>[80;1;Millimeters]</VerticalShift>\n"
    elif sheet_orientation == "Landscape" and feed_direction == "SEF":
        x += "<RotationValue>0</RotationValue>\n"
        x += "<Position>Left</Position>\n"
        x += "<HorizontalShift>[0;1;Millimeters]</HorizontalShift>\n"
        x += "<VerticalShift>[80;1;Millimeters]</VerticalShift>\n"
    elif sheet_orientation == "Landscape" and feed_direction == "LEF":
        x += "<RotationValue>90</RotationValue>\n"
        x += "<Position>Top</Position>\n"
        x += "<HorizontalShift>[80;1;Millimeters]</HorizontalShift>\n"
        x += "<VerticalShift>[0;1;Millimeters]</VerticalShift>\n"
    else:
        print("Something wrong with the sheetorientation or feeddirection")
        exit(1)
    x += "<Side>Front</Side>\n"
    # -----------------------
    x += "</Stamp>\n"
    return x

def add_number(img_file, number2add):
    '''Add a number to the image file'''
    # NOTE: Hardcoded positions
    # Open the barcode
    img = Image.open(img_file)
    orig_width, orig_height = img.size
    dpi_res = img.info['dpi']

    # figure out the colour mode to set the correct bg colour
    mode = img.mode
    if len(mode) == 1:  # L, 1
        new_background = (255)
        txt_col = (0)
    if len(mode) == 3:  # RGB
        new_background = (255, 255, 255)
        txt_col = (0,0,0)
    if len(mode) == 4:  # RGBA, CMYK
        new_background = (255, 255, 255, 255)
        txt_col = (0,0,0,0)
    
    newImage = Image.new(mode, (orig_width, int(orig_height*1.5)), new_background)
    # Center the barcode
    y1 = int(math.floor((newImage.size[1] - orig_height) / 2))
    newImage.paste(img, (0, y1, orig_width, y1 + orig_height))
    # Add the text
    # make a blank image for the text
    fnt = ImageFont.truetype("overpass-mono-semibold.otf", 80)
    text_size = fnt.getsize(number2add)
    txt = Image.new(mode, text_size, new_background)
    # get a drawing context
    d = ImageDraw.Draw(txt)
    d.text((0,0), number2add, font=fnt, fill=txt_col)
    x1 = int(newImage.size[0]/2 - txt.size[0]/2)
    y2 = int(newImage.size[1] - txt.size[1])
    newImage.paste(txt,(x1,y2))
    txt = txt.rotate(180)
    newImage.paste(txt,(x1,0))
    img.close()
    newImage.save("%s" % img_file, dpi=(dpi_res))


if __name__ == "__main__":
    #add_number("test.png", "01")
    main()