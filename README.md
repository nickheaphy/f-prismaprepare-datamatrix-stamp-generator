# Datamatrix Generator

The Tecnau TC1530 Dynamic Perforator for the Oce Varioprint i300/200 is driven using DataMatrix codes. The DataMatrix codes indicate for each sheet, what the perforation program should be applied to that sheet.

The DataMatrix codes should appear on the lead edge of the sheet inside the printable area.

PrismaPrepare can apply the DataMatrix codes using the *Stamp* functionality.

This Python script generates PNG images of the datamatrix codes, with the recommended quiet zone and sized to the suggested size for use on the Tecnau. It then combines all the PNG files together, writes an .XML file describing the position of the DataMatrix on the sheet and then compresses everything together to form a PrismaPrepare Stamp Catalog.

The Stamp Catalog can then be imported into PrismaPrepare.

There is a prebuilt Stamp Catalog included (`PrismaPrepare_Stamp_Catalog.zip`) if you just want to download and use rather than building the catalog from scratch.

## Pre Installation

If you want to run the script yourself rather than just using the `PrismaPreapre_Stamp_Catalog.zip` then you will need to ensure Python3 is installed. On a PC install from https://www.python.org/, on OSX best option is to first install Brew https://brew.sh/ and then `brew install python3`

The code does have a dependency on PIL for image generation, so you will need to install Pillow `python3 -m pip install Pillow`

## Configuration

There are a couple of things that can be changed in `generate_dm_codes.py` if you want different sized or different resolution barcodes.

The position of the barcode is controlled by the XML that is output to the TemporaryStampCatalog.xml file. This position is hardcoded in `generate_dm_codes.py`. If you want the barcode in a different position then the process would be: In PrismaPrepare create a stamp in the appropriate position and then export the stamp catalog. Open the `TemporaryStampCatalog.xml` and copy an paste the position information nodes in the XML into the XML generation in `generate_dm_codes.py`

## Background

This code is based on https://github.com/mmulqueen/pyStrich though I have eliminated everything other than the DataMatrix generation.

Reason for forking this codebase is that I needed to make some changes to the generation code to make the quiet zone configurable (as according to the documentation the Tecnau needs a larger than normal quiet zone) as well as being able to set the DPI in the PNG generation (this in turn sets the size that the barcode appears in PrismaPrepare)

## Updates

29/5/2018 - Code has been changed to add human readable text to the barcode. This does mean that now orientation of the barcode is important so have had to generate two additional stamp configurations in the PrismaPrepare stamp catalog (now there is "Portrait SEF", "Portrait LEF", "Landscape SEF" and "Landscape LEF")

## Assumptions

* ~~The DataMatrix code should appear on the top right of the sheet. This is controlled in the XML in the line <Position>TopRight</Position> so it can be changed to TopLeft, BottomLeft, BottomRight (probably also want to change the <Name>%s Top_Right</Name> to reflect the position)~~ Changed to Left with a +80 vertical shift.

* Cellsize of 0.4mm with a 7 cell quite zone is correct for the Tecnau (this can be set in the script, however need to be careful as i300 has a 2mm unprintable margin and the codes are being positioned with the assumption that the quiet zone is larger than the unprintable margin)